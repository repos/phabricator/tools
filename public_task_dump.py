#!/usr/bin/env python3

import gzip
import json
import os
import sys
from wmfphablib import phabdb
from wmfphablib import config as c

# silence stdout but keep stdout
# previously this script was started with 1>/dev/null
# to achieve the same effect
sys.stdout = open(os.devnull, "w")

# Some transaction types are unsafe to reveal as they
# contain hidden information in their history and possible
# unsafe secrets we have dealt with in the UI context
transactions = ['core:columns',
                'priority',
                'points',
                'status',
                'reassign',
                'core:edge']

# This nonsense is a response to getting bytes from the MySQL interface rather
# than strings in various places:
def decode_bytes(row):
    decoded_row = []
    for field in row:
        if isinstance(field, bytes):
            field = field.decode()
        decoded_row.append(field)
    return tuple(decoded_row)

def dbcon(db):
    return phabdb.phdb(db=db,
                       host=c.dbslave,
                       port=c.slaveport,
                       user=c.phuser_user,
                       passwd=c.phuser_passwd)

mdb = dbcon('phabricator_maniphest')
pdb = dbcon('phabricator_project')

data = {}
taskdata = {}
tasks = phabdb.get_taskbypolicy(mdb)
# cache a list of public phids
task_phids = [id[1] for id in tasks]
# cache a list of project policies
project_policies = {}

count = 0
for task in tasks:
    count = count + 1
    id = task[0]

    taskdata[id] = {}
    taskdata[id]['info'] = decode_bytes(task)
    taskdata[id]['transactions'] = {}

    # Query for all transactions for this task:
    for t in transactions:
        transaction_rows = phabdb.get_transactionbytype(mdb, task[1], t)
        if transaction_rows is not None:
            transaction_rows = tuple(decode_bytes(row) for row in transaction_rows)
        taskdata[id]['transactions'][t] = transaction_rows

    # There are a few types of edge relationships, we want only publicly
    # available relationships:
    edges = phabdb.get_edgebysrc(mdb, task[1])
    if not edges:
        continue

    edge_allowed = []
    for edge in edges:
        if edge[2].startswith(b'PHID-PROJ'):
            if edge[2] not in project_policies:
                project_policies[edge[2]] = phabdb.get_projectpolicy(pdb, edge[2])
            if project_policies[edge[2]] == b'public':
                edge_allowed.append(decode_bytes(edge))
        if edge[2].startswith(b'PHID-TASK'):
            # we compare to a our known good list of public tasks
            if edge[2] in task_phids:
                edge_allowed.append(decode_bytes(edge))
    taskdata[id]['edge'] = list(filter(bool, edge_allowed))

data['task'] = taskdata
data['project'] = {}

project_data = phabdb.get_projectbypolicy(pdb, policy='public')
if project_data is not None:
    data['project']['projects'] = tuple(map(decode_bytes, project_data))
column_data = phabdb.get_projectcolumnsbypolicy(pdb, policy='public')
if column_data is not None:
    data['project']['columns'] = tuple(map(decode_bytes, column_data))

mdb.close()
pdb.close()

with gzip.open('/srv/dumps/phabricator_public.dump.gz', 'w') as f:
    f.write(json.dumps(data).encode('utf8'))
